package com.iwolf.tech;

import com.iwolf.tech.entity.User;
import com.iwolf.tech.repo.UserDao;
import com.iwolf.tech.repo.UserRepo;
import com.iwolf.tech.service.UserService;
import com.iwolf.tech.start.UserServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;


/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-19 17:05
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserServiceApplication.class) // 指定我们SpringBoot工程的Application启动类
@WebAppConfiguration
@TestPropertySource(locations="classpath:application-dev.properties")
public class UserTest {

    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserDao userDao;

     @Test
     public void test(){
         userService.test();
     }

    @Test
    public void userList(){
       List<User> userList=userService.selectList();
       for(User user:userList){

           System.out.println("qq="+user.toString());
       }
    }

    @Test
    public void findUser(){
        User user=userService.findUser(22L);
        System.out.println("user33="+user.toString());
    }

}
