package com.iwolf.tech.controller;

import com.iwolf.tech.service.UserService;
import com.iwolf.tech.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-19 17:55
 **/
@RestController
public class HelloworldController {

    @Autowired
    UserService userService;

    @RequestMapping("/save")
    public void test(){
        User user=new User();
        user.setAge(52);
        user.setName("haha");
        userService.test();
    }

    @RequestMapping("/hello")
    public String hello(){
        return "helloworld!";
    }
}
