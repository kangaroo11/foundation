package com.iwolf.tech.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-19 20:42
 **/
public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {

        //可以做一个简单的负载均衡策略
        String lookupKey = DynamicDataSourceHolder.getDataSource();
        System.out.println("------------lookupKey---------"+lookupKey);

        return lookupKey;
    }
}