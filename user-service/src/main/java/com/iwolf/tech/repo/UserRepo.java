package com.iwolf.tech.repo;

import com.iwolf.tech.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepo extends JpaRepository<User, Long> {


    /*两种注解形式都可以*/
    @Query("from User a where a.id = :id")
//    @Query("select a from User a where a.id = ?1")
    User findUser(@Param("id") Long id);

}
