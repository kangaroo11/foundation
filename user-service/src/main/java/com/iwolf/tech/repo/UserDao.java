package com.iwolf.tech.repo;

import com.iwolf.tech.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-20 17:23
 **/
@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    public User findAuthor(Long id){
        return this.entityManager
                .createQuery("select t from User t where id = ?", User.class)
                .setParameter(1, id)
                .getSingleResult();
    }
}
