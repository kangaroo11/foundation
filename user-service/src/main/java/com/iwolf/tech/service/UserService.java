package com.iwolf.tech.service;

import com.iwolf.tech.config.TargetDataSource;
import com.iwolf.tech.entity.User;
import com.iwolf.tech.inter.service.IUserService;
import com.iwolf.tech.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-19 20:55
 **/
public class UserService implements IUserService {

    @Autowired
    UserRepo userRepo;


    @TargetDataSource(dataSource = "writeDataSource")
    @Transactional(value = "writeTransactionManager")
    public void test(){
        User user=new User();
        user.setAge(77);
        user.setName("haha");
        userRepo.save(user);
    }

    public User findUser(Long id){
       return userRepo.findUser(id);
    }

    @TargetDataSource(dataSource = "writeDataSource")
    @Transactional(value = "writeTransactionManager")
    @Override
    public void save(User user){
        userRepo.save(user);
    }

    @Override
    public List<User> selectList() {
        return userRepo.findAll();
    }
}
