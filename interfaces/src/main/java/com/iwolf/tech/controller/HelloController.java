package com.iwolf.tech.controller;

import com.iwolf.tech.inter.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Desc:用于测试
 * Author:liubo
 * Date: 2017-06-07 15:27
 **/
@Controller
public class HelloController {

    @Autowired
    @Qualifier("testService")
    ITestService testService;

    @ResponseBody
    @RequestMapping(value = "/test")
    public String testEx(String test) throws Exception {
        return testService.test();
    }

//    @ResponseBody
//    @RequestMapping("/testBEx")
//    public String testBEx() throws Exception {
//        throw new BusinessException(PrjErrorEnum.PARAM_INVALID);
//    }

    @ResponseBody
    @RequestMapping("/hello")
    public String hello() throws Exception {
        return "hello_world!";
    }

    @RequestMapping("/index")
    public String index() {
        return "index2";
    }
}
