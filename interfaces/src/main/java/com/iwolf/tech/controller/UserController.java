package com.iwolf.tech.controller;

import com.iwolf.tech.entity.User;
import com.iwolf.tech.inter.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-20 11:20
 **/
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    @Qualifier("userService")
    IUserService userService;

    @RequestMapping("selectAll")
    public List selectAll() {
        List list = userService.selectList();
        return list;
    }

    @RequestMapping("save")
    public void save() {
        User user = new User();
        user.setAge(99);
        user.setName("test");
        userService.save(user);
    }

}
