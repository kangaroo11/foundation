package com.iwolf.tech.config;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-05-11 14:00
 **/

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MyWebAppConfigurer
        extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/files/**").addResourceLocations("file:"+ SysConfig.fileUploadPath+"/");
        super.addResourceHandlers(registry);
    }

}