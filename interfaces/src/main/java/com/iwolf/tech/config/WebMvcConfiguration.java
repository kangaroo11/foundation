package com.iwolf.tech.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-04-24 11:05
 **/
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new EncodingInterceptor());
//        registry.addInterceptor(new AuthInterceptor());
//        registry.addInterceptor(new ExceptionInterceptor());
    }
}
