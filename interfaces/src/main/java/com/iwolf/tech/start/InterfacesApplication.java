package com.iwolf.tech.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * Desc:启动类
 * Author:liubo
 * Date: 2017-03-28 14:31
 **/
@SpringBootApplication
@ComponentScan("com.iwolf.tech")
@ImportResource({"classpath:applicationConsumer.xml"})
public class InterfacesApplication {
    public static void main(String[] args) {
        SpringApplication.run(InterfacesApplication.class,args);
    }
}
