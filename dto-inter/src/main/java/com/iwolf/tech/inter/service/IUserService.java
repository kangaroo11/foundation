package com.iwolf.tech.inter.service;

import com.iwolf.tech.entity.User;

import java.util.List;

/**
 * Desc:
 * Author:liubo
 * Date: 2017-07-20 10:20
 **/
public interface IUserService {

    public void save(User user);

    public List<User> selectList();

}
